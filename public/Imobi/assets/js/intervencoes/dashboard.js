$(document).ready(function(){

	function getUrlDashboard() {
		wUrl = $("#sa-baseurl").val() + "dashboard";
		return wUrl;
	}

	function sleep(milliseconds) {
		var start = new Date().getTime();
		for (var i = 0; i < 1e7; i++) {
			if ((new Date().getTime() - start) > milliseconds){
				break;
			}
		}
	}

	$(".add-new-space").click(function(){

		wUrlAddSpace = getUrlDashboard();
		wUrlAddSpace += "/spaces/add";

		$.getJSON(wUrlAddSpace,function(callback){
			b = new Boxy(callback.content, {modal: true, unloadOnHide: true, title:"&nbsp;"});
			_sa_formatButtons();
			b.center();

			objContent = b.getContent();
			objContent.find("form").find("input[name='nome']").focus();
			objContent.find("form").submit(function(){
				objSerialize = $(this).serialize();
				spaceNome = $(this).find("input[name='nome']").val();
				$.post(wUrlAddSpace, objSerialize, function(callbackPost){
					if (callbackPost.type == "sucess") {
						b.hide();
						spaceIndex = ($(".spaces-nav").find("ul").find("li[class^='active-spaces-added']").length+1);
						spaceId = callbackPost.space;
						lispace = "<li class='active-spaces-added fnt-sansl'><a id='spaced-" + spaceIndex + "' href='javascript:void(0)' alt='" + spaceId + "," + spaceIndex + "' class='open-space-area active-spaces-l'><span><i class='icon-circle-blank'></i> &nbsp;" + spaceNome + "</span></a></li>";
						divspace = "<div class='space-" + spaceId + "'></div>";
						$(".spaces-nav").find("ul").find("li:last-child").before(lispace);
						$(".dashboard-spaces").find("div[class^='space-']:last-child").before(divspace);
						spaceOpenArea(spaceIndex);
					}
					$.jGrowl(callbackPost.msg, {
                        title: "Atenção"
                    });
				}, "json");
			});

		});

	});

	function spaceOpenArea(spaceIndex) {

		$(".open-space-area").unbind();
		$(".open-space-area").click(function(){

			objAltParams = $(this).attr("alt").replace(",","/");

			objParamsSpace = $(this).attr("alt").split(",");
			objAlt = objParamsSpace[0];

			if (objParamsSpace[1] != undefined) {
				objIdBase = objParamsSpace[1];
			} else {
				objIdBase = 0;
			}

			window.location.hash = "spcd-" + objIdBase;

			$("div[class^='space-']").each(function(index, element){
				$(element).hide().removeClass("space-selected");
				className = $(this).attr("class");
				if (className != "space-0" && className != "space-loading") {
					$(element).html("");
					$(element).html("<br><br><h1 align='center'><i class='icon-refresh' style='font-size:36px'></i><br>Aguarde ... carregando dados.</h1>");
				}
			});

			objData = $("div[class='space-" + objAlt + "']");

			$(".open-space-area").find("span").removeClass();
			$(".open-space-area").find("span").find("i").removeClass().addClass("icon-circle-blank");

			$(this).find("span").removeClass().addClass("c-blue");
			$(this).find("span").find("i").removeClass().addClass("icon-circle");

			$(".spaces-nav").find("li").find("a").removeClass("space-selected");
			$(this).addClass("space-selected");

			if (objAlt != 0) {

				objData.addClass("space-selected");
				objData.show();

				$.ajaxq("space-queue");

				//$(".space-loading").clone().show().appendTo("div[class^='space-" + objAlt + "']");

				wUrlOpenSpace = getUrlDashboard();
				wUrlOpenSpace += "/spaces/open/" + objAltParams;

				$.ajaxSetup({
				    async: true
				});

				$.get(wUrlOpenSpace, function(callbackOpenSpace){

					$(objData).html(callbackOpenSpace);
					_sa_formatButtons();

					optionsSpaceArea();

				});

			} else {
				objData.show();
			}

		});

		if (spaceIndex) {
			$("a[id='spaced-" + spaceIndex + "']").trigger("click");
		}

	}
	spaceOpenArea();

	function hoverSpacesOptions() {
		$("div[class^='spaces-lines-col-']").mouseover(function() {
			$(this).find(".spaces-lines-colbar").find("li").show();
		}).mouseout(function() {
			$(this).find(".spaces-lines-colbar").find("li").hide();
		});
	}

	function optionsSpaceArea() {

		hoverSpacesOptions();

		$(".space-edit").on("click", function(){

			objSpace = $(this);
			objIdSpaceParams = $(this).attr('alt');
			objIdSpace = $(this).attr('alt').replace("/",",");

			wUrlEditSpace = getUrlDashboard();
			wUrlEditSpace += "/spaces/edit/" + objIdSpaceParams;

			$.getJSON(wUrlEditSpace,function(callback){
				b = new Boxy(callback.content, {modal: true, unloadOnHide: true, title:"&nbsp;"});
				_sa_formatButtons();
				b.center();

				objContent = b.getContent();
				objNome = objContent.find("form").find("input[name='nome']");
				objNome.focus();
				objContent.find("form").submit(function(){
					objSerialize = $(this).serialize();
					objSpaceName = objContent.find("form").find("input[name='nome']").val();
					$.post(wUrlEditSpace, objSerialize, function(callbackPost){
						if (callbackPost.type == "sucess") {
							b.hide();
							$(".spaces-nav").find("a[alt='" + objIdSpace + "']").find("span").html("<i class='icon-circle'></i> &nbsp;" + objSpaceName);
						}
						$.jGrowl(callbackPost.msg, {
	                        title: "Atenção"
	                    });
					}, "json");
				});

			});

		});

		$(".space-remove").on("click", function(){

			objSpace = $(this);
			objIdSpaceParams = $(this).attr('alt');
			objIdSpace = $(this).attr('alt').replace("/",",");

			objAltParam = objIdSpace.split(",");

			Boxy.confirm("Tem certeza que deseja remover o espaço ?", function(){
				wUrlRemoveSpace = getUrlDashboard();
				wUrlRemoveSpace += "/spaces/remove/" + objIdSpaceParams;
				$.post(wUrlRemoveSpace,{space: objAltParam[0]}, function(callbackPost){
					if (callbackPost.type == "sucess") {
						$(".spaces-nav").find("a[alt='" + objIdSpace + "']").parent().remove();
						$("a[id='spaced-0']").trigger("click");
					}
					$.jGrowl(callbackPost.msg, {
                        title: "Atenção"
                    });
				}, "json");
			});

		});

		$(".space-mode-edit").toggle(function(){
			$(this).addClass("c-blue");
			$("div[class^='spaces-lines-col-']").find(".spaces-lines-colbar").find("li").show();
			$("div[class^='spaces-lines-col-']").unbind("mouseover mouseout");
		}, function(){
			$("div[class^='spaces-lines-col-']").find(".spaces-lines-colbar").find("li").hide();
			$(this).removeClass("c-blue");
			hoverSpacesOptions();
		});

		$(".edit-widget").on("click", function(){
			objSpaceValue = $(this).attr("alt");

			wUrlSpaceLineAdd = getUrlDashboard();
			wUrlSpaceLineAdd += "/spaces/widgets/edit/" + objSpaceValue;

			$.getJSON(wUrlSpaceLineAdd, function(callback){

				b = new Boxy(callback.content, {modal: true, unloadOnHide: true, title:"&nbsp;"});
				_sa_formatButtons();
				b.center();

				objContent = b.getContent();
				objForm = objContent.find("form");

				objForm.find("select[name='tipo']").change(function(){

					objTipo = $(this).val();

					wUrlWidgets = getUrlDashboard();

					if (objTipo == "1") {
						wUrlWidgets += "/spaces/widgets/shared/" + objTipo;
						$.getJSON(wUrlWidgets, function(callbackWidgets){
							objOptionsWidgets = objContent.find("form").find("select[name='ref']");
							objIdRef = objForm.find("input[name='id_ref']").val();
							$(callbackWidgets).each(function(index, element){
								showSelectedOption = (objIdRef == element.id_graph) ? "selected='selected'" : "";
								objOption = "<option value='" + element.id_graph + "'" + showSelectedOption + ">" + "(" + element.tipo + ") &nbsp;" + element.nome + "</option>";
								objOptionsWidgets.append(objOption);
							});
						});
					}

				});
				objForm.find("select[name='tipo']").trigger("change");

				objForm.submit(function(){
					objSerialize = $(this).serialize();
					$.post(wUrlSpaceLineAdd, objSerialize, function(callbackPost){
						if (callbackPost.type == "sucess") {
							b.hide();
							$(".space-selected").trigger("click");
						}
						$.jGrowl(callbackPost.msg, {
	                        title: "Atenção"
	                    });
					}, "json");
				});

			});

		});

		$(".space-line-add").on("click", function(){
			objSpaceValue = $(this).attr("alt");

			wUrlSpaceLineAdd = getUrlDashboard();
			wUrlSpaceLineAdd += "/spaces/lines/add/" + objSpaceValue;

			$.getJSON(wUrlSpaceLineAdd, function(callback){
				b = new Boxy(callback.content, {modal: true, unloadOnHide: true, title:"&nbsp;"});
				_sa_formatButtons();
				b.center();

				objContent = b.getContent();
				objContent.find("form").submit(function(){
					objSerialize = $(this).serialize();
					$.post(wUrlSpaceLineAdd, objSerialize, function(callbackPost){
						if (callbackPost.type == "sucess") {
							b.hide();
						}
						$.jGrowl(callbackPost.msg, {
	                        title: "Atenção"
	                    });
					}, "json");
				});

			});

		});

		$.ajaxSetup({
		    async: false
		});
		$("div[class^='spaces-line-row']").each(function(index, element){

			$(element).find("div[class^='spaces-lines-col-']").each(function(indexCol, elementCol){

				objAltData = $(elementCol).attr("title");

				wUrlWidgetLoad = getUrlDashboard();
				wUrlWidgetLoad += "/spaces/widgets/load/" + objAltData;

				objPlot = $(elementCol).find(".spaces-lines-content");
				$.ajaxq ("space-queue", {
				    url: wUrlWidgetLoad,
				    dataType: "json",
				    success: function(callback2)
				    {
						objNameBase = "area-show-graph-" + callback2.linha + "-" + callback2.coluna;
				        bo = "<div id='" + objNameBase + "' style='height:20.5em;width:100%'></div>";
				        $(objPlot).html(bo);
				        if (callback2 != null && callback2.jd != undefined) {
				        	var chart = _sa_graphs_render_plot_clean(callback2.jd, false, bo, objNameBase);
				        }
				    }
				});

			});

		});

	}

	if (window.location.hash != "") {
		hash = window.location.hash.replace("#spcd-","");
		$("a[id^='spaced-"+hash+"']").trigger("click");
    } else {
        $("a[id='spaced-0']").trigger("click");
    }

});