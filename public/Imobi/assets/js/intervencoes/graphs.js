
/*
 * Metódo para carregar a tela de inicio da ferramenta de gráficos.
 */
function _sa_graphs_begin(objectWindow, b) {
    $.get(_sa_baseUrl() + "opts/graphs/inicio", function(callback){
        $(objectWindow).html(callback);
    });
}

function _sa_graphs_open(id, chart) {
    // load all connections
    wUrl = _sa_baseUrl();
    wUrl += "opts/graphs/shared/open/" + objId;

    $.getJSON(wUrl, function(callback2){

        bo = new Boxy("<div>" + callback2.c + "</div>", {title: "Visualizando gráfico", modal : true, unloadOnHide: true, beforeUnload: function(){

            if (chart != false) {
                chart.destroy();
            }

        }});
        objContentViewer = bo.getContent();

        //_sa_setDesignButtons();
        $(".button").button();

        objContentViewer.find("a#visualizarDados").click(function(){

            wUrlViewData = _sa_baseUrl();
            wUrlViewData += "opts/graphs/shared/open/data/" + objId;

            $.get(wUrlViewData, function(callbackData){
                boData = new Boxy(callbackData, {title: "Visualizando dados", modal: true, unloadOnHide: true});
                objContentGrid = boData.getContent();
                objContentGrid.find(".grid-show-results").flexigrid({width:"700"});

                boData.center();
            });

        });

        var chart = _sa_graphs_render(callback2.jd, false, bo);

        // process graph request

    });
}

function _sa_graphs_plot(id, chart, objPlot) {

    // load all connections
    wUrl = _sa_baseUrl();
    wUrl += "opts/graphs/shared/open/" + id;

    $.getJSON(wUrl, function(callback2){

        bo = "<div id='area-show-graph2' style='height:300px;'></div>";
        $(objPlot).html(bo);
        //chart.destroy();
        //console.log(callback2);
        var chart = _sa_graphs_render_plot(callback2.jd, false, bo);

        // process graph request

    });
}

function _sa_graphs_call_chart_open(t) {

    objId = $(t).attr("alt");
    chart = "";

    $(".graphs-list a").css({"font-weight":"normal"});

    //$(t).css({"font-weight":"bold"});

    _sa_graphs_open(objId, chart);

}

function _sa_graphs_call_chart_plot(t, objPlot) {
    objId = $(t).val();
    chart = "";

    if (isNaN(objId)) {
        return false;
    }

    _sa_graphs_plot(objId, chart, objPlot);
}

/*
 * Metódo disponível por carregar a area que exibe todos os gráficos compartilhados para o usuário
 */
function _sa_graphs_shared() {
    $.get(_sa_baseUrl() + "opts/graphs/shared", function(callback){

        $(objectWindow).html(callback);

        $(objectWindow).find("table").find("td").click(function(){
            objId = $(this).parent().attr("id");
            chart = "";
            _sa_graphs_open(objId, chart);
            return false;
        });
    });
}

function _sa_graphs_render(jsonReturnData, typeArea, b) {

    if (jsonReturnData == undefined) {
        return false;
    }

    $("#" + jsonReturnData.chart.renderTo).css({"width":"700px","height":"450px","margin":"5px"});
    objContent = b.getContent();
    objContent.find("#visualizarDados").css({"margin-left":"0px"});
    b.center();

    showType = "number";
    cType = (jsonReturnData.chart.defaultSeriesType != undefined) ? jsonReturnData.chart.defaultSeriesType : "pie" ;

    switch (showType) {
        case "number":
            defaultData = {
                tooltip: {
                    formatter: function() {

                        stra = "";
                        switch(cType){
                            case "pie":
                                stra = '<b>'+ this.point.name +'</b> (' + this.point.y + ')';
                            break;
                            case "bar":
                                stra = '<b>'+ this.x +'</b> (' + this.y + ')';
                            break;
                            case "line":
                                stra = '<b>'+ this.point.category +'</b> (' + this.y + ')';
                            break;
                        }

                        return stra;
                    }
                },
                legend: {
                    itemStyle: {
                        cursor: 'pointer',
                        color: '#545454'
                    },
                    itemMarginTop: 10,
                    itemMarginBottom: 10,
                    borderWidth: 2,
                    borderColor: '#dadada',
                    backgroundColor: '#ffffff',
                    enabled: true
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: true,
                            formatter: function() {
                                var ptage = this.y;
                                return '<b>'+ this.point.name +'</b> ('+ ptage +')';
                            }
                        }
                    }
                }
            }
        break;
        case "percentage":
            defaultData = {
                tooltip: {
                    formatter: function() {

                        ptag = this.percentage;
                        stra = "";
                        switch(cType){
                            case "pie":
                                stra = '<b>'+ this.point.name +'</b> ' + ptag.toFixed(2) + '%';
                            break;
                            case "bar":
                                stra = '<b>'+ this.x + '</b> (' + this.y + ')';
                            break;
                            case "line":
                                stra = '<b>'+ this.point.name +'</b> (' + this.y + ')';
                            break;
                        }

                        return stra;
                    }
                },
                legend: {
                    itemStyle: {
                        cursor: 'pointer',
                        color: '#545454'
                    },
                    itemMarginTop: 10,
                    itemMarginBottom: 10,
                    borderWidth: 2,
                    borderColor: '#dadada',
                    backgroundColor: '#ffffff',
                    enabled: true
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: true,
                            formatter: function() {

                                var ptag = this.percentage;

                                stra = "";
                                switch(cType){
                                    case "pie":
                                        stra = '<b>'+ this.point.name +'</b> ' + ptag.toFixed(2) + '%';
                                    break;
                                    case "bar":
                                        stra = '<b>'+ this.x + '</b> (' + this.y + ')';
                                    break;
                                    case "line":
                                        stra = '<b>'+ this.point.name +'</b> (' + this.y + ')';
                                    break;
                                }

                                return stra;
                            }
                        }
                    }
                }
            }
        break;
    }

    data = jsonReturnData;

    dataAttrs = $.extend(defaultData, data);

    chart1 = new Highcharts.Chart(dataAttrs);

    return chart1;

}

function _sa_graphs_render_plot(jsonReturnData, typeArea, b, objName) {

    if (objName == undefined) {
        objName = "area-show-graph2";
    }

    jsonReturnData.chart.renderTo = objName;
    showType = "number";
    cType = (jsonReturnData.chart.defaultSeriesType != undefined) ? jsonReturnData.chart.defaultSeriesType : "pie" ;

    switch (showType) {
        case "number":
            defaultData = {
                tooltip: {
                    formatter: function() {

                        stra = "";
                        switch(cType){
                            case "pie":
                                stra = '<b>'+ this.point.name +'</b> (' + this.point.y + ')';
                            break;
                            case "bar":
                                stra = '<b>'+ this.x +'</b> (' + this.y + ')';
                            break;
                            case "line":
                                stra = '<b>'+ this.point.category +'</b> (' + this.y + ')';
                            break;
                        }

                        return stra;
                    }
                },
                legend: {
                    itemStyle: {
                        cursor: 'pointer',
                        color: '#545454'
                    },
                    itemMarginTop: 10,
                    itemMarginBottom: 10,
                    borderWidth: 2,
                    borderColor: '#dadada',
                    backgroundColor: '#ffffff',
                    enabled: true
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: true,
                            formatter: function() {
                                var ptage = this.y;
                                return '<b>'+ this.point.name +'</b> ('+ ptage +')';
                            }
                        }
                    }
                }
            }
        break;
        case "percentage":
            defaultData = {
                tooltip: {
                    formatter: function() {

                        ptag = this.percentage;
                        stra = "";
                        switch(cType){
                            case "pie":
                                stra = '<b>'+ this.point.name +'</b> ' + ptag.toFixed(2) + '%';
                            break;
                            case "bar":
                                stra = '<b>'+ this.x + '</b> (' + this.y + ')';
                            break;
                            case "line":
                                stra = '<b>'+ this.point.name +'</b> (' + this.y + ')';
                            break;
                        }

                        return stra;
                    }
                },
                legend: {
                    itemStyle: {
                        cursor: 'pointer',
                        color: '#545454'
                    },
                    itemMarginTop: 10,
                    itemMarginBottom: 10,
                    borderWidth: 2,
                    borderColor: '#dadada',
                    backgroundColor: '#ffffff',
                    enabled: true
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: true,
                            formatter: function() {

                                var ptag = this.percentage;

                                stra = "";
                                switch(cType){
                                    case "pie":
                                        stra = '<b>'+ this.point.name +'</b> ' + ptag.toFixed(2) + '%';
                                    break;
                                    case "bar":
                                        stra = '<b>'+ this.x + '</b> (' + this.y + ')';
                                    break;
                                    case "line":
                                        stra = '<b>'+ this.point.name +'</b> (' + this.y + ')';
                                    break;
                                }

                                return stra;
                            }
                        }
                    }
                }
            }
        break;
    }

    data = jsonReturnData;

    dataAttrs = $.extend(defaultData, data);

    chart1 = new Highcharts.Chart(dataAttrs);

    return chart1;

}


function _sa_graphs_render_plot_clean(jsonReturnData, typeArea, b, objName) {

    if (objName == undefined) {
        objName = "area-show-graph2";
    }

    jsonReturnData.chart.renderTo = objName;
    showType = "number";
    cType = (jsonReturnData.chart.defaultSeriesType != undefined) ? jsonReturnData.chart.defaultSeriesType : "pie" ;

    switch (showType) {
        case "number":
            defaultData = {
                tooltip: {
                    formatter: function() {

                        stra = "";
                        switch(cType){
                            case "pie":
                                stra = '<b>'+ this.x +'</b> (' + this.point.y + ')';
                                //stra = false;
                            break;
                            case "bar":
                                stra = '<b>'+ this.x +'</b> (' + this.y + ')';
                            break;
                            case "line":
                                stra = '<b>'+ this.point.category +'</b> (' + this.y + ')';
                            break;
                        }

                        return stra;
                    }
                },
                legend: {
                    itemStyle: {
                        cursor: 'pointer',
                        color: '#545454'
                    },
                    itemMarginTop: 10,
                    itemMarginBottom: 10,
                    borderWidth: 2,
                    borderColor: '#dadada',
                    backgroundColor: '#ffffff',
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: false,
                        cursor: 'pointer',
                        showInLegend: false,
                        dataLabels: {
                            enabled: true,
                            formatter: function() {
                                var ptage = this.y;
                                return '<b>'+ this.point.name +'</b> ('+ ptage +')';
                            }
                        }
                    }
                }
            }
        break;
        case "percentage":
            defaultData = {
                tooltip: {
                    formatter: function() {

                        ptag = this.percentage;
                        stra = "";
                        switch(cType){
                            case "pie":
                                stra = '<b>'+ this.point.name +'</b> ' + ptag.toFixed(2) + '%';
                            break;
                            case "bar":
                                stra = '<b>'+ this.x + '</b> (' + this.y + ')';
                            break;
                            case "line":
                                stra = '<b>'+ this.point.name +'</b> (' + this.y + ')';
                            break;
                        }

                        return stra;
                    }
                },
                legend: {
                    itemStyle: {
                        cursor: 'pointer',
                        color: '#545454'
                    },
                    itemMarginTop: 10,
                    itemMarginBottom: 10,
                    borderWidth: 2,
                    borderColor: '#dadada',
                    backgroundColor: '#ffffff',
                    enabled: true
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: true,
                            formatter: function() {

                                var ptag = this.percentage;

                                stra = "";
                                switch(cType){
                                    case "pie":
                                        stra = '<b>'+ this.point.name +'</b> ' + ptag.toFixed(2) + '%';
                                    break;
                                    case "bar":
                                        stra = '<b>'+ this.x + '</b> (' + this.y + ')';
                                    break;
                                    case "line":
                                        stra = '<b>'+ this.point.name +'</b> (' + this.y + ')';
                                    break;
                                }

                                return stra;
                            }
                        }
                    }
                }
            }
        break;
    }

    data = jsonReturnData;

    dataAttrs = $.extend(defaultData, data);

    chart1 = new Highcharts.Chart(dataAttrs);

    return chart1;

}

function _sa_reports_open(t) {

    id = $(t).attr("alt");
    bo = new Boxy("<div>Aguarde ... carregando ...</div>",{modal: true, title:"&nbsp;",unloadOnHide: true});

    wUrl = _sa_baseUrl();
    wUrl += "/opts/reports/view/" + id;

    $.getJSON(wUrl, function(callback){

        if (callback.type == "args") {

            bo.setContent(callback.data);
            bo.center();

        } else if (callback.type == "load") {

            bo.hide();
            window.open(callback.url, "_blank");

        }

        $.jGrowl(callback.msg, {
            title: "Atenção"
        });

    });

}