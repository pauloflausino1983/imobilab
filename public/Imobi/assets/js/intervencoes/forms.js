function $idEntifica(id) {
	return document.getElementById(id);
}
 
if (form == undefined) var form = {};
form = function() {};

form.prototype = {
	
	settings: function(highlightColor,actHighlight,typeAlert,alertBox,messageTitulo,messageRodape) {
		this.highlightColor = highlightColor;
		this.actHighlight = actHighlight;
		this.alertBox = alertBox;
		this.typeAlert = typeAlert;
		this.messageTitulo = messageTitulo;
		this.messageRodape = messageRodape;
	},
	
	// inicia o form e diz quais os tipos de campos que irão ser validados e quais serão proibidos
	camposNulos: function(id_form,tipoCampo,campoProibido) {
		this.id_form = id_form;
		objFormulario = $idEntifica(this.id_form);
		this.j=0;
		var arrayTipos = tipoCampo.split("|");
		var camposProibidos = campoProibido.split("|");
		this.wMensagem = "";
		if (this.typeAlert == "box") { 
			this.tipoSeparador = "<br>"; 
			this.tipoDestaque_inicio = "<b>";
			this.tipoDestaque_fim = "</b>";
		} else { 
			this.tipoDestaque_inicio = "";
			this.tipoDestaque_fim = "";
			this.tipoSeparador = "\n"; 
		}
		for (x=0; x < arrayTipos.length; x++) {
			for (i=0; i < objFormulario.elements.length; i++) {
				if (objFormulario.elements[i].disabled == false) {
	                if (objFormulario.elements[i].type == arrayTipos[x] && objFormulario.elements[i].name != camposProibidos[x] || objFormulario.elements[i].id == arrayTipos[x]) {
						objFormularioElem = objFormulario.elements[i].className;
						objElem_sep = objFormularioElem.split(" ");
						for (w=0; w < objElem_sep.length; w++) {
							if (this.formRegras(objElem_sep[w],objFormulario.elements[i])){
								this.j++;
								this.wMensagem += objFormulario.elements[i].title + this.tipoSeparador;						
								if (this.actHighlight) {
									objFormulario.elements[i].style.backgroundColor = this.highlightColor;
								}							
							} else {
								objFormulario.elements[i].style.backgroundColor = "#FFFFFF";
							}
						}
					}
				}
			}
		}
		if (this.j > 0) {
			this.mensagem();
			return false;
		} else {
			this.escondeMensagem();
			return true;
		}
	},
	
	// regras de validação, qualquer coisa cria uma regra nova aqui.
	formRegras: function(rulerType,obj) {
		switch(rulerType) {
			case "valida-nulo":
				if (obj.value == "") { 
					return true
				} else { 
					return false
				} 
			break;
			case "valida-email":
                var valEmail = /^[\w-]+(\.[\w-]+)*@(([A-Za-z\d][A-Za-z\d-]{0,61}[A-Za-z\d]\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
				if (valEmail.test(obj.value)) {
					return false;
				} else {
					return true;
				}
			break;
			case "valida-msn":
				var valEmail = /^[\w-]+(\.[\w-]+)*@(([A-Za-z\d][A-Za-z\d-]{0,61}[A-Za-z\d]\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
				if (valEmail.test(obj.value)) {
					return false;
				} else {
					return true;
				}
			break;
			case "valida-twitter":
				var valTwitter = /^@/igm;
				if (valTwitter.test(obj.value)) {
					return false;
				} else {
					return true;
				}
			break;
			case "valida-caracters":
				if (obj.value == "") { 
					return true;
				} else { 
					var valEmail = /\W/;
					if (valEmail.test(obj.value)) {
						return true;
					} else {
						return false;
					}
				}
			break;
			case "valida-email-on-nulo":
				if (obj.value != "") {
					var valEmail = /^[\w-]+(\.[\w-]+)*@(([A-Za-z\d][A-Za-z\d-]{0,61}[A-Za-z\d]\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
					if (valEmail.test(obj.value)) {
						return false;
					} else {
						return true;
					}
				}
			break;
			case "valida-data":
				var vDate = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
				if (vDate.test(obj.value)) {
					return false;
				} else if (!vDate.test(obj.value) || obj.value == "") {
					return true
				}
			break;
			case "valida-numero":
				if (obj.value != "") {
					return (isNaN(obj.value) && !/^\s+$/.test(obj.value));
				} else {
					return true;
				}
			break;
			case "valida-moeda":
				reMoeda = /^\d{1,3}(\.\d{3})*\,\d{2}$/;
				if (obj.value != "" && reMoeda.test(obj.value)) {
					return false;
				} else {
					return true;
				}
			break;
			case "valida-url":
				valUrl = /^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i;
				if (obj.value != "" && valUrl.test(obj.value)) {
					return false;
				} else {
					return true;
				}
			break;
			case "valida-cpf":
				var i;
 				s = obj.value;
				var c = s.substr(0,9);
				var dv = s.substr(9,2);
				var d1 = 0;
				for (i = 0; i < 9; i++) {
					d1 += c.charAt(i)*(10-i);
				}
				if (d1 == 0) {
					return true;
				}
				d1 = 11 - (d1 % 11);
				if (d1 > 9) d1 = 0;
				if (dv.charAt(0) != d1) {
					return true;
				}
				d1 *= 2;
				for (i = 0; i < 9; i++) {
					d1 += c.charAt(i)*(11-i);
				}
				d1 = 11 - (d1 % 11);
				if (d1 > 9) d1 = 0;
				if (dv.charAt(1) != d1) {
					return true;
				}
				return false; 
			break;
			case "valida-cnpj":
				var cnpj = obj.value;;
			    var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
			    var dig1= new Number;
			    var dig2= new Number;
			    
			    exp = /\.|\-|\//g
			    cnpj = cnpj.toString().replace( exp, "" );
			    var digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));
			        
			    for(i = 0; i<valida.length; i++){
			        dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);    
			        dig2 += cnpj.charAt(i)*valida[i];    
			    }
			    dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
			    dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));
			    
			    if(((dig1*10)+dig2) != digito)    
			        return true
			break;
			case "valida-duplo-password":
				
				pipoca = $idEntifica(this.id_form);
				var arr = "";
				for (t=0; t < pipoca.elements.length; t++) {
                    if (pipoca.elements[t].className == "valida-duplo-password" || pipoca.elements[t].className == "button valida-duplo-password") {
						
						objForm = $("#" + this.id_form + " input=[type='password']");
						
						objSenha1 = $(objForm[0]).val();
                        objSenha2 = $(objForm[1]).val();
						
                        if (objSenha1 != "") {
                            if (objSenha1 != objSenha2 || objSenha1 == "" || objSenha2 == "") {
                                return true
                            } else {
                                return false
                            }
                        } else {
							return true;
						}
						
					}
				}
			break;
            case "valida-multiple-noselected":
                if (obj.options.length > 0) {
                    return false;
                } else {
                    return true;
                }
            break;
            case "valida-checked":
                if (obj.checked == false) {
                	return true;
                } else {
                	return false;
                }
            break;
		}
	},
	
	// parte para criar a mensagem
	mensagem: function() {
		wMensagem_cabecalho = this.tipoDestaque_inicio + this.messageTitulo + this.tipoDestaque_fim + this.tipoSeparador + this.tipoSeparador;
		if (this.messageRodape != "") { 
			wMensagem_rodape = this.tipoSeparador + this.messageRodape; 
		} else {
			wMensagem_rodape = ""; 
		}
		
		this.mensagemFinal = wMensagem_cabecalho + this.wMensagem + this.tipoSeparador + wMensagem_rodape;
		if (this.typeAlert == "box") {
			objDivAlert = $idEntifica(this.alertBox);
			objDivAlert.innerHTML = this.mensagemFinal	
			objDivAlert.style.display = "block";
		} else if (this.typeAlert == "alert") {
			alert(this.mensagemFinal);
		}
		
		return false;
	},
	
	// esconder painel de mensagem
	escondeMensagem: function() {
		objDivAlert = $idEntifica(this.alertBox);
		objDivAlert.innerHTML = "";
		objDivAlert.style.display = "none";
	}

}