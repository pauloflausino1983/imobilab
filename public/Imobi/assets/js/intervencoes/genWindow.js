(function($){

    /*
     * Plugin para manipulação de janelas
     * @param customAttrs object
     * @param callback function
     */
    $.fn.genWindowCreate = function(customAttrs, callback) {

        objId = false

        if (customAttrs.title == undefined) { title = false; } else { title = customAttrs.title; }
        if (customAttrs.content == undefined) { content = false; } else { content = customAttrs.content; }

        // onload window properties
        owp = {
            main : objId,
            title : title,
            tabs : true,
            content: customAttrs.content,
            boxy : {
                title : title,
                showTitle : true,
                unloadOnHide : true,
                closeText: "<i class='icon-remove'></i>",
                modal: false
            }
        }

        genWindowOpts = $.extend(true, owp, customAttrs);

        w = new Boxy(genWindowOpts.content, genWindowOpts.boxy);

        if (typeof callback == "function") {

            objCallBack = {
                data : genWindowOpts.content,
                window : w
            }

            callback.call(this, objCallBack);

        }

        return false;

    },

    $.fn.genWindowCrudCreate = function(input ,customAttrs, link, callbackRequest, callbackPost) {

        if (callbackRequest == undefined) { callbackRequest = false; }
        if (callbackPost == undefined) { callbackPost = false; }

        defaultAttrs = {
            title : false,
            modal : false,
            unloadOnHide: true,
            msg : {
                loading : "Aguarde carregando ..."
            }
        }

        opts = $.extend(true, defaultAttrs, customAttrs);

        function setContentBoxy(c) {
            return "<div id='window-content-app' style='width:700px;max-height:600px;overflow:auto;'>" + c + "</div>";
        }

        Serialize = false;

        function formatFormAjax(f, b) {

            b1 = b.getContent();

            f.unbind("submit");
            _sa_ContentFormRequiredFields(f, b);
            _sa_AutoCompleteLoad(f);
            //alert("oie");
            f.submit(function(){

                objSerialize = $(this).serialize();

                if (Serialize == objSerialize) { return false; }
                Serialize = objSerialize;

                $.post(link, objSerialize, function(callback2){
                    b.setContent(setContentBoxy(callback2.data));
                    b2 = b.getContent();

                    _sa_loadComponents(b2);
                    _sa_desactiveButtonSubmitAfterAction(b2);

                    formatFormAjax(b2.find("form"), b);

                    b2.find("#showOptionalFields").click(function(){
                        b.center();
                    });

                    b.center();

                    // callback
                    if (typeof callbackPost == "function") {

                        objCallBack = {
                            data : callback2.data,
                            req : callback2.request,
                            window : b
                        }

                        callbackPost.call(this, objCallBack);

                    }

                    return false;

                }, "json");

                return false;

            });

        }

        bo = new Boxy("<div>" + opts.msg.loading + "</div>", opts);

        $.getJSON(link, function(callback){

            bo.setContent(setContentBoxy(callback.data));

            b2 = bo.getContent();

            _sa_loadComponents(b2);
            _sa_desactiveButtonSubmitAfterAction(b2);

            if (input) {
                objForm = b2.find(input);
                formatFormAjax(objForm, bo);
            }

            bo.center();

            b2.find("#showOptionalFields").click(function(){
                bo.center();
            });

            formatTipsy();

            // callback
            if (typeof callbackRequest == "function") {

                objCallBack = {
                    data : callback.data,
                    req : callback.request,
                    window : bo
                }

                callbackRequest.call(this, objCallBack);

            }

            return false;

        });

    }

    $.fn.genWindow = function(customAttrs, callback) {

	$(this).click(function(){

        objId = $(this).attr("id");

	    defaultAttrsClick = {
		main : objId,
		title : $(this).attr("title"),
		href : $(this).attr("href"),
		showIn : objId + "-content",
		showLoading: true,
            boxy : {
                title : $(this).attr("title"),
                showTitle : true,
                unloadOnHide : true,
                closeText: "<i class='icon-remove'></i>"
            }
        }

	    genWindowOpts = $.extend(true, defaultAttrsClick, customAttrs);

	    // load the window
	    $.get(genWindowOpts.href,function(data){

            objectReturn = (data.type != undefined) ? data.type : false;

            if (!objectReturn) {

                if (!genWindowOpts.boxy.showTitle) { genWindowOpts.boxy.title = false; }

                b = new Boxy(data, genWindowOpts.boxy);

                if (typeof callback == "function") {

                    objCallBack = {
                        data : data,
                        window : b
                    }

                    callback.call(objCallBack);

                }

            }


	    });

	    return false;

	});

    }
})(jQuery);