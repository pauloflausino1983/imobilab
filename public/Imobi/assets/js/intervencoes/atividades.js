function formAtividades(t) {

    objForm = $("form[name^='fp_atividades_']");
    objForm.find("select[name='pref_auto_inicio_rp_ac']").parent().prepend("<br><span class='sm-padding sm-color-gray'><b>REGRAS DE AUTOMAÇÕES</b></span><br><br>");
    objForm.find("select[name='pref_site_atualizar_identificacao']").parent().prepend("<br><span class='sm-padding sm-color-gray'><b>OBRIGATORIEDADE NO CADASTRO DE SITE</b></span><br><br>");
    objForm.find("select[name='pref_interv_atualizar_ident']").parent().prepend("<br><span class='sm-padding sm-color-gray'><b>OBRIGATORIEDADE NO CADASTRO DE INTERVENÇÃO</b></span><br><br>");


    objForm.find("select[name='pref_email_auto_inicio_rp_ac']").parent().prepend("<br><span class='sm-padding sm-color-gray'><b>NOTIFICAÇÕES POR E-MAIL</b></span><br><br>");
    objForm.find("select[name='pref_incluir_anexo']").parent().prepend("<br><span class='sm-padding sm-color-gray'><b>OBRIGATORIEDADE DE ANEXO</b></span><br><br>");

    btnClose = t.window.getContent().find("a");

    btnClose.removeAttr("onclick").unbind("click").click(function(){
        Boxy.confirm("<h2>Notificação</h2><span class='tlf-cor-azul-padrao'>Tem certeza que deseja fechar a janela?</span>", function(){
            t.window.hide();
        });
        return false;
    });

}
