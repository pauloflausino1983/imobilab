
function windowGrupos(d, objGrupos) {

    objWindowNovoGrupo = $(d.window.getContent());
    owng = objWindowNovoGrupo;

    owng.find("input[name='nome']").focus();
    owng.find(".markCheckBoxes").find("img").toggle(function(){
        owng.find("input[type='checkbox']").removeAttr("checked");
        $(this).attr({title:"Marcar todos"});
    },function(){
        owng.find("input[type='checkbox']").each(function(index,element){
            if ($(this).attr("name") == "action[]") {
                $(element).attr({"checked": "checked"});
            }
        });
        $(this).attr({title:"Desmarcar todos"});
    }).tipsy({gravity:"s"}).css({cursor:"pointer"});

    owng.find("form").submit(function(){

        $error=0;
        objForm = owng.find("form");
        inputNome = objForm.find("input[name='nome']");
        checkBoxes = objForm.find("input[type='checkbox']");
        if (!inputNome.val().match(/\S/)) {
            $.jGrowl("É necessário informar o nome do grupo",{
                header:"Atenção", life: 10000
            });
            $error=1;
        }

        // verifica se ao menos uma opção foi selecionada.
        if ($error==0) {
            checked=0;
            checkBoxes.each(function(){
                if ($(this).attr("checked") && checked==0 && $error==0) {
                    checked=1;
                }
            });

            if (checked==0) {
                $.jGrowl("É necessário selecionar no mínimo uma ação que o grupo '" + inputNome.val() + "' terá permissão para acessar",{
                    header:"Atenção", life: 10000
                });
                $error=1;
            }

        }
        if ($error==0) {

            // realiza um serialize de todos os itens do formulário e prossegue com o post das informações.
            objSerializeForm = objForm.serialize();
            $.post(objForm.attr("action"), objSerializeForm, function(data){
                switch(data.type) {
                    case "sucess":

                        $.jGrowl(data.msg,{
                            header:"Atenção", life: 10000
                        });

                        if (objForm.find("input[name='manterJanelaAberta']:checked").length == 0) {
                            d.window.hide();
                            objGrupos.parent().parent().find("#sm-window-header-usuarios").find("ul.list-options").find("li.selected a").trigger("click");
                        } else {
                            if (objForm.find("input[name='onreset']").length > 0 &&  objForm.find("input[name='onreset']").val() != "false") {
                                $(objForm).each(function(){ this.reset(); });
                            }
                            objGrupos.parent().parent().find("#sm-window-header-usuarios").find("ul.list-options").find("li.selected a").trigger("click");
                        }

                    break;
                    case "error":

                        $.jGrowl(data.msg,{
                            header:"Atenção", life: 10000
                        });

                    break;
                }
            }, "json");
        }

        return false;

    });

}

function installedRemove() {

    $('.installed-remove').click(function(){
        objParent = $(this).parent().parent().parent();
        objAlt = $(this).attr("alt").split(",");
        wUrl = _sa_baseUrl() + "apps/instalado/remover/" + objAlt[0] + "/" + objAlt[1];
        boxyInstalled = new Boxy("<div>Aguarde ... carregando ...</div>", {title: "&nbsp;", modal: true, unloadOnHide: true});
        $.get(wUrl, function(callback){
            boxyInstalled.setContent(callback);
            boxyInstalled.center();

            boxyContent = boxyInstalled.getContent();

            $(".button").button();

            boxyContent.find(".go-to-uninstall").click(function(){

                remove_data = (boxyContent.find("input[name='remove_data']:checked").val() != undefined) ? 1 : 0;

                $.post(wUrl, {app: objAlt[0], schema: objAlt[1], remove_data: remove_data}, function(callbackPost){

                    if (callbackPost.type == "sucess") {

                        $.jGrowl(callbackPost.msg,{header:"Atenção", life: 10000});
                        boxyInstalled.hide();

                        //$(".apps-area-menu").find("a:eq(0)").trigger("click");
                        window.location.reload();

                    } else {
                        $.jGrowl(callbackPost.msg,{header:"Atenção", life: 10000});
                    }

                }, "json");

            });
        });
    });

}

function reinstallApp() {
    $(".reinstall-app").click(function(){
        objAlt = $(this).attr("alt");
        Boxy.confirm("Tem certeza que deseja <b>reinstalar</b> o aplicativo", function(){
            window.location = objAlt;
        }, {title: "Atenção"});
    });
}

function dataFilesApp() {
    $(".grid-datafiles").flexigrid();
    $(".download-datafile").click(function(){
        objAlt = $(this).attr("alt");
        Boxy.confirm("Tem certeza que deseja baixar o arquivo <b>" + objAlt + "</b> ?", function(){
            boxyConfirm = new Boxy("<div>Aguarde ...</div>",{title: "&nbsp;", modal: true, unloadOnHide: true});
            wUrl = _sa_baseUrl() + "apps/index/extraidos/download/" + objAlt;
            windowDownload = window.open(wUrl,"_blank");
            if (windowDownload) {
                intervalSet = self.setInterval(function datacheck(){
                    if (windowDownload.closed) {
                        $.jGrowl("O Arquivo será baixado para o seu computador. Mantenha-o <b>ligado</b> até o <b>término do download</b>.",{header:"Atenção", life: 10000});
                        boxyConfirm.hide();
                        self.clearInterval(intervalSet);
                    }
                }, 100);
            }
        },{title:"Atenção"});
    });
}

/** FIELDS / Dictionary */
function fieldsDict() {

    $(".fields-dict").unbind("click").click(function(){

        objAlt = $(this).attr("alt");

        bo = new Boxy("<div>Aguarde ... carregando ...</div>", {title: "&nbsp;", modal : true, unloadOnHide: true});

        wUrl = _sa_baseUrl() + "apps/fields/dict/" + objAlt;

        $.get(wUrl, function(callback){

            bo.setContent(callback);
            bo.center();

            fieldsDictOptions($(bo.getContent()));

        });


    });

}

function fieldsDictOptions(objContent) {

    $(".fields-dict-open-form").unbind("click").click(function(){

        objAlt = $(this).attr("alt").split("/");

        bof = new Boxy("<div>Aguarde ... carregando ...</div>", {title: "&nbsp;", modal : true, unloadOnHide: true});

        wUrl = _sa_baseUrl() + "apps/fields/dict/list";

        $.post(wUrl,{modulo:objAlt[0],action:objAlt[1]}, function(callback){

            bof.setContent(callback);
            bof.center();

            fieldsDictEdit($(bo.getContent()), bof);

        });

    });

}

function fieldsDictEdit(objArea, bof){

    $("a[class^='fields-dict-edit-']").unbind("click").click(function(){

        bof.center();

        objAltDict = $(this).attr("alt").split("/");
        objThis = $(this);

        objThis.hide();

        objFielDescription = $(".area-dict-" + objAltDict[2]).find(".fieldDescription").val();

        objTextarea = "<p><textarea name='description' maxlength='120' style='border:1px solid #e1e1e1' class='sm-unit-100'>" + objFielDescription + "</textarea></p><br>";
        objFieldSubmit = "<p><a href='javascript:;;' alt='" + objAltDict[2] + "' class='sm-buttons-flat-blue save-action'>SALVAR</a> | <a href='javascript:;;' alt='" + objAltDict[2] + "' class='cancel-action'>Cancelar</a></p>";
        objFieldHidden = "<input type='hidden' name='modulo' value='" + objAltDict[0] + "'/> <input type='hidden' name='action' value='" + objAltDict[1] + "'/> <input type='hidden' name='key' value='" + objAltDict[2] + "'/>";

        objFormDict = "<form name='dict-" + objAltDict[2] + "' onsubmit='return false;'>" + objTextarea + objFieldSubmit + objFieldHidden + "</form>";

        objAreaDict = $("#area-" + objAltDict[2]);

        objAreaDict.html(objFormDict);

        objAreaDict.find("textarea[name='description']").text(objFielDescription).focus();

        objAreaDict.find(".cancel-action").unbind("click").click(function(){

            objAlt = $(this).attr("alt");

            bDesc = $(".area-dict-" + objAlt).find(".fieldDescription").val();

            $("#area-" + objAlt).html(bDesc);

            $(".fields-dict-edit-" + objAlt).show();

            bof.center();

        });

        objAreaDict.find("form[name='dict-" + objAltDict[2] + "']").submit(function(){

            objSerialize = $(this).serialize();

            dictKey = $(this).find("input[name='key']").val();

            wUrl = _sa_baseUrl() + "apps/fields/dict/edit";
            $.post(wUrl, objSerialize, function(callback){

                $.jGrowl(callback.msg,{ header:"Atenção" });

            }, "json");

        });

        objAreaDict.find(".save-action").unbind("click").click(function(){

            objAlt = $(this).attr("alt");

            oar = $("#area-" + objAlt);
            oar.find("form[name='dict-" + objAlt + "']").trigger("submit");

            tval = oar.find("textarea").val();

            descNew = (!tval.match(/\S/)) ? "" : tval;
            oar.text(descNew);

            $(".area-dict-" + objAlt).find(".fieldDescription").val(descNew);

            $(".fields-dict-edit-" + objAlt).show();

            bof.center();

        });

    });

}

/** IMPORT DATA */
function importData() {

    $(".window-import-data").unbind("click").click(function(){

        objAlt = $(this).attr("alt");

        bo = new Boxy("<div>Aguarde ... carregando ...</div>", {title: "&nbsp;", modal : true, unloadOnHide: true});

        wUrl = _sa_baseUrl() + "apps/data/" + objAlt;

        $.get(wUrl, function(callback){

            bo.setContent(callback);
            bo.center();

            objContent = bo.getContent();

            importDataOptions(objContent, objAlt);

        });


    });

}

function importDataOptions(objContent, altRefs) {

    $(".import-data-menu").find("a").unbind("click").click(function(){

        objAlt = $(this).attr("alt");

        wUrlImport = _sa_baseUrl() + "apps/data/" + objAlt + "/" + altRefs;

        objContent.find(".data-area-content").html("<p align='center'>Aguarde ... carregando...</p>");

        $.get(wUrlImport, function(callback){

            objContent.find(".data-area-content").html(callback);

            dataAreaContent = $(".data-area-content");

            switch(objAlt) {
                case "import":
                    importDataSubOptionsImport(dataAreaContent, altRefs);
                break;
                case "files":
                    importDataSubOptionsFiles(dataAreaContent, altRefs);
                break;
                case "export":
                    importDataSubOptionsExport(objContent, altRefs);
                break;
            }

        });

    });

}

function importDataSubOptionsExport(objContent, altRefs) {

    objContent.find("form[name='form-export-data']").submit(function(){

        formSelected = objContent.find("select[name='formSelected']").val();
        formatDataFile = objContent.find("select[name='formatDataFile']").val();
        indexesImport = objContent.find("select[name='indexesImport']").val();
        chainingData = objContent.find("select[name='chainingData']").val();

        intervIni = objContent.find("input[name='interv_ini']").val();
        intervEnd = objContent.find("input[name='interv_end']").val();

        wUrlExportData = _sa_baseUrl() + "apps/data/export/download/" + altRefs + "/" + formSelected + "/" + indexesImport + "/" + formatDataFile + "/" + chainingData + "/" + intervIni + "/" + intervEnd;
        window.open(wUrlExportData, '_blank');

        return false;

    });

    objContent.find(".export-download-data").unbind("click").click(function(){
        objContent.find("form[name='form-export-data']").trigger("submit");
    });

}

function importDataSubOptionsImport(objContent, altRefs) {

    objContent.find("select[name='formSelected']").change(function(){

        formSelected = $(this).val();
        formatDataFile = objContent.find("select[name='formatDataFile']").val();
        clearDataForm = objContent.find("select[name='clearOldData']").val();
        indexesImport = objContent.find("select[name='indexesImport']").val();

        console.log(formSelected);

        //actionStricted = objVal.replace("_","-");
        //console.log(actionStricted);
        // get details about form
        wUrlDetailsForm = _sa_baseUrl() + "apps/data/import/details/" + altRefs + "/" + formatDataFile + "/" + indexesImport;
        wUrlUploadFile = _sa_baseUrl() + "apps/data/import/upload/" + altRefs + "/" + formatDataFile + "/" + clearDataForm + "/" + formSelected + "/" + indexesImport;

        $.post(wUrlDetailsForm,{action:formSelected}, function(callback){

            $(".area-upload-file").html(callback);

            $("#multiple-file").unbind();
            $("#multiple-file").html5Uploader({
                name: "Filedata",
                postUrl: wUrlUploadFile,
                onSuccess: function callbackSuccess(e, file, response, fieldHandler) {
                    $(".area-import-file").html(response);
                    $(".area-import-file").find(".di-show-uploaded-files").click(function(){
                        $(".import-data-menu a[alt='files']").trigger("click");
                    });
                    $(".area-import-file").find(".di-import-opt").click(function(){
                        $(".import-data-menu a[alt='import']").trigger("click");
                    });
                },
                onClientLoadStart: function callbackLoadStart(e, file) {

                }
            });

        });

    });

    objContent.find("select[name='formatDataFile']").unbind("change").change(function(){
        objContent.find("select[name='formSelected']").trigger("change");
    });

    objContent.find("select[name='clearOldData']").unbind("change").change(function(){
        objContent.find("select[name='formSelected']").trigger("change");
    });

    objContent.find("select[name='indexesImport']").unbind("change").change(function(){
        objContent.find("select[name='formSelected']").trigger("change");
    });

}

function importDataSubOptionsFiles(objContent, altRefs) {

    $(".import-files-open-formatacao").unbind("click").click(function(){

        idImport = $(this).attr("alt");

        boOpenFormatDetails = new Boxy("<div>Aguarde ... carregando informações.</div>", {modal: true, title: "&nbsp;", unloadOnHide: true, closeable: false});

        wUrlOpenFormatacao = _sa_baseUrl() + "apps/data/files/options/" + idImport;

        $.get(wUrlOpenFormatacao, function(callbackArea){

            boOpenFormatDetails.setContent(callbackArea);
            boOpenFormatDetails.center();

            objContent = boOpenFormatDetails.getContent();

            objForm = objContent.find("form[name='dataFileOption']");

            objForm.submit(function(){

                wUrlSaveFormatacao = _sa_baseUrl() + "apps/data/files/options/save/" + idImport;

                objSerialize = $(this).serialize();

                $.post(wUrlSaveFormatacao, objSerialize, function(callbackFormat){

                    if (callbackFormat.type == "sucess") {
                        boOpenFormatDetails.hide();
                        $(".import-data-menu").find("a[alt='files']").trigger("click");
                    }

                    $.jGrowl(callbackFormat.msg,{header:"Atenção"});

                }, "json");

                return false;

            });

            objForm.find(".trigger-submit-format").unbind("click").click(function(){
                objForm.trigger("submit");
            });


        });

    });

    $(".import-files-cancel").unbind("click").click(function(){

        idImport = $(this).attr("alt");

        Boxy.confirm("Tem certeza que deseja cancelar a importação do arquivo ? ", function(callback){

            wUrlCancelFile = _sa_baseUrl() + "apps/data/files/cancel/" + idImport;

            $.post(wUrlCancelFile, {id_import: idImport}, function(callbackCancel){

                if (callbackCancel.type == "sucess") {
                    $(".import-data-menu").find("a[alt='files']").trigger("click");
                }

                $.jGrowl(callbackCancel.msg,{header:"Atenção"});

            }, "json");

        });

        return false;

    });

    $(".import-files-confirm").unbind("click").click(function(){

        idImport = $(this).attr("alt");

        Boxy.confirm("Tem certeza que deseja confirmar a importação do arquivo ? ", function(callback){

            wUrlConfirmFile = _sa_baseUrl() + "apps/data/files/confirm/" + idImport;

            $.post(wUrlConfirmFile, {id_import: idImport}, function(callbackConfirm){

                if (callbackConfirm.type == "sucess") {
                    $(".import-data-menu").find("a[alt='files']").trigger("click");
                }

                $.jGrowl(callbackConfirm.msg,{header:"Atenção"});

            }, "json");

        });

        return false;

    });

    $(".import-files-history").unbind("click").click(function(){

        idImport = $(this).attr("alt");

        wUrlHistory = _sa_baseUrl() + "apps/data/files/history/" + idImport;

        boOpenFilesHistory = new Boxy("<div>Aguarde ... carregando informações.</div>", {modal: true, title: "Histórico", unloadOnHide: true, closeable: true});

        $.get(wUrlHistory, function(callback){
            boOpenFilesHistory.setContent(callback);
            boOpenFilesHistory.center();
        });

    });

}

function exportData() {

}

function storeApp() {
    $(".menu-store").find("ul.menu-store-index a").unbind("click");
    $(".menu-store").find("ul.menu-store-index a").click(function(){

        objAltStore = $(this).attr("alt");

        $(".menu-store").find("ul.menu-store-index li, ul.menu-store-categorias li").removeClass("bg-gray-opt");
        $(this).parent().addClass("bg-gray-opt");

        wUrlStoreOpt = _sa_baseUrl() + "apps/index/store/" + objAltStore;

        $.get(wUrlStoreOpt, function(callbackArea){
            $(".apps-show").html(callbackArea);
            setOptionsViewApps();
        });

    });
    $(".menu-store").find("ul.menu-store-categorias a").click(function(){

        objAltStore = $(this).attr("alt");
        $(".menu-store").find("ul.menu-store-index li, ul.menu-store-categorias li").removeClass("bg-gray-opt");
        $(this).parent().addClass("bg-gray-opt");
        wUrlStoreOpt = _sa_baseUrl() + "apps/index/store/categoria/" + objAltStore;

        $.get(wUrlStoreOpt, function(callbackArea){
            $(".apps-show").html(callbackArea);
            setOptionsViewApps();
        });

    });
    $(".menu-store").find("ul.menu-store-index a:eq(0)").trigger("click");

}

function setOptionsViewApps() {

    $(".apps-show").find("li.open-app-details").unbind("click");
    $(".apps-show").find("li.open-app-details").click(function(){
        appClicked = $(this).find("input[name='app']").val();
        bo = new Boxy("<div>Aguarde ... carregando informações.</div>", {modal: true, title: "&nbsp;", unloadOnHide: true});

        wUrlOpenAppDetails = _sa_baseUrl() + "apps/index/store/open/details/app/" + appClicked;

        $.get(wUrlOpenAppDetails, function(callbackArea){
            //$(bo.getContent()).html(callbackArea);
            bo.setContent(callbackArea);
            bo.center();
            optionsAppDetails();
            //hs.updateAnchors();
        });

    });
}

function optionsAppDetails() {

    $(".btn-install").click(function(){
        btninstall = $(this);
        btninstall.attr({"disabled":"disabled"});
        wUrlInstall = _sa_baseUrl() + "sas" + "/process";
        objAppCod = $("input[name='StoreAppSelected']").val();
        $.post(wUrlInstall, {app : objAppCod}, function(callbackInstall){
            if (callbackInstall.type == "error") {
                alert(callbackInstall.msg);
                btninstall.removeAtrr("disabled");
            } else {
                window.location = callbackInstall.url;
            }
        }, "json");
    });
}

$(document).ready(function(){

    $(".apps-area-menu").find("a").unbind("click");
    $(".apps-area-menu").find("a").click(function(){

        objAlt = $(this).attr("id");

        window.location.hash = objAlt;

        wUrl = _sa_baseUrl() + "apps/index/" + objAlt;

        $.get(wUrl, function(callbackArea){

            $(".apps-area-content").html(callbackArea);
            $(".button").css({"color":"gray"}).button();

            loadWindowUsuariosPermissions();
            installedRemove();
            reinstallApp();
            dataFilesApp();
            storeApp();
            fieldsDict();
            importData();

            _sa_formatTipsy();

            $(".cancelar-remocao-app").click(function(){
                objAlt = $(this).attr("alt");
                wUrlRemocaoCancelar = _sa_baseUrl() + "apps/index/remocao/cancelar";
                Boxy.confirm("Tem certeza que deseja cancelar a remoção do aplicativo ?", function(){
                    $.post(wUrlRemocaoCancelar, {uninstall: objAlt}, function(callbackRemocaoCancelar){
                        if (callbackRemocaoCancelar.type == "sucess") {
                            $.jGrowl(callbackRemocaoCancelar.msg,{header:"Atenção", life: 10000});
                            $(".apps-area-menu").find("a:eq(0)").trigger("click");
                        } else {
                            $.jGrowl(callbackRemocaoCancelar.msg,{header:"Atenção", life: 10000});
                        }
                    }, "json");
                },{title:"Atenção"});
            });

            $(".reenviar-codigo-remocao").click(function(){
                objAlt = $(this).attr("alt");
                wUrlRemocaoCancelar = _sa_baseUrl() + "apps/index/remocao/reenviar/codigo";
                Boxy.confirm("Tem certeza que deseja gerar um novo código para validar a remoção do aplicativo ?", function(){
                    $.post(wUrlRemocaoCancelar, {uninstall: objAlt}, function(callbackRemocaoCancelar){
                        if (callbackRemocaoCancelar.type == "sucess") {
                            $.jGrowl(callbackRemocaoCancelar.msg,{header:"Atenção", life: 10000});
                            $(".apps-area-menu").find("a:eq(0)").trigger("click");
                        } else {
                            $.jGrowl(callbackRemocaoCancelar.msg,{header:"Atenção", life: 10000});
                        }
                    }, "json");
                },{title:"Atenção"});
            });

            $(".smart-lock-app").unbind("click").click(function(){
                objAlt = $(this).attr("alt").split(",");
                wUrlLock = _sa_baseUrl() + "apps/lock";
                Boxy.confirm("Tem certeza que deseja bloquear o acesso ao aplicativo ?", function(){
                    $.post(wUrlLock, {app: objAlt[0], schema: objAlt[1]}, function(callbackLock){
                        if (callbackLock.type == "sucess") {
                            $.jGrowl(callbackLock.msg,{header:"Atenção", life: 10000});
                            $(".apps-area-menu").find("a:eq(0)").trigger("click");
                        } else {
                            $.jGrowl(callbackLock.msg,{header:"Atenção", life: 10000});
                        }
                    }, "json");
                },{title:"Atenção"});
            });

            $(".smart-unlock-app").unbind("click").click(function(){
                objAlt = $(this).attr("alt");
                wUrlUnlock = _sa_baseUrl() + "apps/unlock";
                Boxy.confirm("Tem certeza que deseja desbloquear o acesso ao aplicativo ?", function(){
                    $.post(wUrlUnlock, {id_lock: objAlt}, function(callbackLock){
                        if (callbackLock.type == "sucess") {
                            $.jGrowl(callbackLock.msg,{header:"Atenção", life: 10000});
                            $(".apps-area-menu").find("a:eq(0)").trigger("click");
                        } else {
                            $.jGrowl(callbackLock.msg,{header:"Atenção", life: 10000});
                        }
                    }, "json");
                },{title:"Atenção"});
            });

            $("form[name='liberar-remocao']").submit(function(){

                objForm = $(this);

                objForm.find("input[type='submit']").val("Aguarde ...");


                wUrlRemocao = _sa_baseUrl() + "apps/index/remocao/validate";
                objSerialize = $(this).serialize();
                $.post(wUrlRemocao,objSerialize, function(callbackRemocao){

                    if (callbackRemocao.type == "sucess") {
                        $.jGrowl(callbackRemocao.msg,{header:"Atenção", life: 10000});
                        $(".apps-area-menu").find("a:eq(0)").trigger("click");
                    } else {
                        $.jGrowl(callbackRemocao.msg,{header:"Atenção", life: 10000});
                    }
                    objForm.find("input[type='submit']").val("Liberar remoção");
                }, "json");

            });

        });
    });

    if (window.location.hash != "") {
        hash = window.location.hash.replace("#", "");
        $(".apps-area-menu").find("#" + hash).trigger("click");
    } else {
        $(".apps-area-menu").find("li:first-child a").trigger("click");
    }

});

function loadWindowUsuariosPermissions() {

    $("a.window-usuarios").each(function(){
        objHref= $(this).attr("alt");

        $(this).genWindow({boxy:{modal:true, title: "Permissões de acesso"}, href: objHref},function(){
            bwindow = this;

            $(bwindow.data).find("ul.list-options").genTabs({
                window: "#sm-window-usuarios",
                boxy : bwindow.window,
                showIn: "#sm-window-content-usuarios",
                menu : {
                    create : true,
                    nodes : {
                        grupos : {
                            0 : {
                                "texto" : "<b class='c-blue'><i class='icon-plus'></i> &nbsp;Adicionar novo grupo</b>",
                                "classe" : "addnovogrupo"
                            }
                        }
                    }
                }
            }, function(){

                objContentTab = bwindow.window.getContent();

                content = objContentTab;
                content.find(".cb-enable").click(function(){

                    if (confirm("Você tem certeza que deseja confirmar a ação ?")) {

                        var parent = $(this).parents('.switch');
                        $('.cb-disable',parent).removeClass('selected');
                        $(this).addClass('selected');
                        $('.checkbox',parent).attr('checked', true);
                        appSelected = $('.hidden',parent).val();
                        schemaSelected = $("#sm-window-details-usuarios").find("input[name='schemaSelected']").val();

                        wUrlPermissionAdd = $("#sa-baseurl").val() + "apps/permissoes/aplicativo/adicionar/" + appSelected + "/" + schemaSelected;
                        $.post(wUrlPermissionAdd, {usuario: $('.checkbox',parent).val()}, function(callback2) {
                            $.jGrowl(callback2.msg,{
                                header:"Atenção"
                            });
                        }, "json");

                    }

                });
                content.find(".cb-disable").click(function(){
                    if (confirm("Você tem certeza que deseja remover o acesso do usuário ?")) {

                        var parent = $(this).parents('.switch');
                        $('.cb-enable',parent).removeClass('selected');
                        $(this).addClass('selected');
                        $('.checkbox',parent).attr('checked', false);
                        appSelected = $('.hidden',parent).val();
                        schemaSelected = $("#sm-window-details-usuarios").find("input[name='schemaSelected']").val();
                        modUserSelected = $('.hiddenModUserSelected', parent).val();
                        wUrlPermissionAdd = $("#sa-baseurl").val() + "apps/permissoes/aplicativo/remover/" + appSelected + "/" + schemaSelected;
                        $.post(wUrlPermissionAdd, {usuario: $('.checkbox',parent).val(), modUserSelected: modUserSelected}, function(callback2) {
                            $.jGrowl(callback2.msg,{
                                header:"Atenção"
                            });
                        }, "json");

                    }
                });

                objGrupos = $(objContentTab).find("#gruposList");
                objUsuarios = $(objContentTab).find("#usersList");

                baseUrl = _sa_baseUrl();

                objUsuarios.find(".modificarPermissoes").click(function(){
                    params = $(this).attr("alt").split(".");
                    wUrlModificarPermissoes = baseUrl + "apps/usuarios/permissoes/" + params[0] + "/" + params[1] + "/" + params[2];
                    $.get(wUrlModificarPermissoes, function(callback){
                        $(this).genWindowCreate({content: callback, title : "Modificar permissões", boxy : {modal: true, draggable: true}}, function(data2){

                            objWindowEditPermissoes = data2.window.getContent();

                            _sa_formatButtons();

                            objWindowEditPermissoes.find(".markCheckBoxes").find("img").toggle(function(){
                                objWindowEditPermissoes.find("input[type='checkbox']").removeAttr("checked");
                                $(this).attr({title:"Marcar todos"});
                            },function(){
                                objWindowEditPermissoes.find("input[type='checkbox']").each(function(index,element){
                                    if ($(this).attr("name") == "action[]") {
                                        $(element).attr({"checked": "checked"});
                                    }
                                });
                                $(this).attr({title:"Desmarcar todos"});
                            }).tipsy({gravity:"s"}).css({cursor:"pointer"});

                            $(".tipsy-s").tipsy({gravity:"s"}).css({cursor:"pointer"});

                            objFormPermissions = $(objWindowEditPermissoes).find("form[name='modificarPermissoes']");
                            objFormPermissions.submit(function(){
                                objSerialize = $(this).serialize();
                                wUrlEditPermissoes = $(this).attr("action");
                                $.post(wUrlEditPermissoes,objSerialize,function(returnEditPermissoes){
                                    rep = returnEditPermissoes;
                                    switch(rep.type) {
                                        case "sucess":
                                            $.jGrowl(returnEditPermissoes.msg,{header:"Atenção", life: 10000});
                                            if (objFormPermissions.find("input[name='manterJanelaAberta']:checked").length == 0) {
                                                data2.window.hide();
                                            }
                                        break;
                                        case "error":
                                            $.jGrowl(returnEditPermissoes.msg,{header:"Atenção", life: 10000});
                                        break;
                                    }
                                }, "json");
                            });
                        });
                    });
                });

                objUsuarios.find(".removerAcesso").click(function(){
                    params = $(this).attr("alt").split(".");
                    Boxy.confirm("Deseja confirmar a exclusão do acesso deste usuário ao aplicativo?", function() {

                        wUrlRemoverAcesso = baseUrl + "apps/usuarios/remover/acesso/" + params[0] + "/" + params[1];
                        $.getJSON(wUrlRemoverAcesso, function(callback){
                            cb = callback;
                            switch(cb.type) {
                                case "sucess":
                                    bwindow.window.hide();
                                    $.jGrowl(cb.msg,{header:"Atenção", life: 10000});
                                break;
                                case "error":
                                    $.jGrowl(cb.msg,{header:"Atenção", life: 10000});
                                break;
                            }
                        });
                        return false;
                    }, {title: 'Atenção'});
                });

                objUsuarios.find(".associarGrupo").click(function(){
                    params = $(this).attr("alt").split(".");
                    wUrlModificarPermissoes = baseUrl + "apps/usuarios/grupo/" + params[0] + "/" + params[1] + "/" + params[2];
                    $.get(wUrlModificarPermissoes, function(callback){
                        $(this).genWindowCreate({content: callback, title : "Associar grupo", boxy : {modal: true, draggable: true}}, function(data2){
                            objWindowContent = data2.window.getContent();
                            objForm = $(objWindowContent).find("form[name='usuariosPermissoesGrupo']");
                            _sa_formatButtons();
                            objForm.submit(function(){
                                wUrlUsuariosPermissoesGrupo = baseUrl + "apps/usuarios/grupo/" + params[0] + "/" + params[1] + "/" + params[2];
                                objVars = objForm.serialize();
                                $.post(wUrlUsuariosPermissoesGrupo, objVars, function(cup){
                                    switch(cup.type) {
                                        case "sucess":
                                            data2.window.hide();
                                            $.jGrowl(cup.msg,{header:"Atenção", life: 10000});
                                        break;
                                        case "error":
                                            $.jGrowl(cup.msg,{header:"Atenção", life: 10000});
                                        break;
                                    }
                                }, "json");
                            });
                        });
                    });
                });

                // LISTAR GRUPOS
                objGrupos.find("#listGrupos").each(function(){

                    // EDITAR GRUPO
                    $(this).find(".editargrupo").click(function(){

                        wUrlNovoGrupo = baseUrl + "apps/usuarios/grupos/editar/" + $(this).attr("alt");
                        objThis = this;
                        $.get(wUrlNovoGrupo, function(callbackNovoGrupo){
                            $(this).genWindowCreate({content: callbackNovoGrupo, title : "Editar grupo", boxy : {modal: true, draggable: true}}, function(data2){
                                windowGrupos(data2, objGrupos);
                                _sa_formatButtons();
                            });
                        });
                    });

                    // APAGAR GRUPO
                    $(this).find(".apagargrupo").click(function(){
                        arrayIdGrupo = $(this).parent().parent().attr("id").split("-");
                        id_grupo = arrayIdGrupo[1];
                        nomegrupo = $(this).parent().parent().find('.gruponome').text();
                        Boxy.confirm("Tem certeza que deseja apagar o grupo '" + nomegrupo + "' ?", function() {

                            wUrlApagarGrupo = baseUrl + "apps/usuarios/grupos/apagar/" + id_grupo;

                            $.getJSON(wUrlApagarGrupo, function(data){

                                $.jGrowl(data.msg,{header:"Atenção", life: 10000});

                                switch(data.type) {
                                    case "sucess":
                                        objGrupos.parent().parent().find("#sm-window-header-usuarios").find("ul.list-options").find("li.selected a").trigger("click");
                                    break;
                                }

                            });
                        }, {title: 'Apagar grupo'});
                        return false;
                    });


                });

                // CRIAR NOVO GRUPO
                objGrupos.parent().parent().find("#criarNovoGrupo, .addnovogrupo").each(function(){

                    $(this).click(function(){
                        /*
                        if ($(this).attr("alt") == undefined) {
                            wAlt = objGrupos.find("input[name='appNome']").val();
                        } else {
                            wAlt = $(this).attr("alt")
                        }
                        */
                        wAlt = objGrupos.find("input[name='appNome']").val() + "." + objGrupos.find("input[name='appSchema']").val();
                        baseUrl = _sa_baseUrl();
                        objAlt = wAlt.split(".");
                        wUrlNovoGrupo = baseUrl + "apps/usuarios/grupos/novo/" + objAlt[0] + "/" + objAlt[1];

                        $.get(wUrlNovoGrupo, function(callbackNovoGrupo){

                            objGrupos.find("#criarNovoGrupo").genWindowCreate({content: callbackNovoGrupo, title : "Novo grupo", boxy : {modal: true, draggable: true}}, function(data2){
                                //$(data2.content).find(".genTable").genTable();
                                windowGrupos(data2, objGrupos);
                                _sa_formatButtons();
                            });

                        });
                        return false;
                    });
                });

                _sa_formatButtons();

            });
        });

    });

}