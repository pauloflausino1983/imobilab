function alertLockWindow(sMsg) {
    alertMsg = "<h2>Notificação</h2><div class='sm-padding'><p align='center'><h4 class='cor-azul-claro' align='center' style='padding-top:8px !important;'>" + sMsg + "</h4></div>";
    Boxy.alert(alertMsg, false);
}

function loadingBoxMsgInterv() {
    return "<div class='paddingBox'><br><div class='tlfLoading' style='margin: 0 auto;'></div><br><p align='center'>Carregando informações</p><br></div>";
}

function tlfShowLockWindow(obj) {

    rb = (obj == undefined) ? $(".lock-window") : obj;

    if (Boxy.linkedTo(rb.get(0)) == undefined || Boxy.linkedTo(rb.get(0)) == false) {
        rb.trigger("click");
    }

}

function tlfHideLockWindow() {

    if ($(".lock-window").get(0) == undefined) {
        return false;
    }

    if (Boxy.linkedTo($(".lock-window").get(0)) != undefined && Boxy.linkedTo($(".lock-window").get(0)) != false) {
        Boxy.linkedTo($(".lock-window").get(0)).hide();
    }
}

function tlfLoadLockWindow() {
    $(".lock-window").unbind("click").click(function(){
        ele = $(this).get(0);
        new Boxy(loadingBoxMsgInterv(),{title:"", modal: true, closeable: false, unloadOnHide: true, actuator: ele});
    });
}

function formResponsaveis(cg) {
    objForm = $("form[name^='fp_responsaveis_']");
    objForm.find("input[name='telefone_fixo']").parent().show().removeClass("field-nonrequired").addClass("field-required");
    objForm.find("input[name='telefone_celular']").parent().show().removeClass("field-nonrequired").addClass("field-required");
    preventHideWindow(cg);
}

function formatTipsy() {
    $(".tipsy-s").tipsy({gravity:'s', html: true});
    $(".tipsy-w").tipsy({gravity:'w', html: true});
    $(".tipsy-e").tipsy({gravity:'e', html: true});
    $(".tipsy-n").tipsy({gravity:'n', html: true});
}

function preventHideWindow(cg) {

    btnClose = cg.window.getContent().find("a");

    btnClose.removeAttr("onclick").unbind("click").click(function(){
        Boxy.confirm("<h2>Notificação</h2><span class='tlf-cor-azul-padrao'>Tem certeza que deseja fechar a janela?</span>", function(){
            cg.window.hide();
        });
        return false;
    });

}

formatTipsy();

function formatGrids() {
    $(".grid-small-smaller").each(function(){
        $(this).flexigrid({
            height:'50',
            width:'auto',
            striped:true,
            singleSelect: false,
            resizable: false,
            onSuccess: function() {
                addGrid($(".grid-small-smaller"), this);
            }
        });
    });
    $(".grid-small-height").each(function(){
        $(this).flexigrid({
            height:'100',
            width:'auto',
            striped:true,
            singleSelect: false,
            resizable: false,
            onSuccess: function() {
                addGrid($(".grid-small-height"), this);
            }
        });
    });
    $(".grid-small-medium").each(function(){
        $(this).flexigrid({
            height:'200',
            width:'auto',
            striped:true,
            singleSelect: false,
            resizable: false,
            onSuccess: function() {
                addGrid($(".grid-small-medium"), this);
            }
        });
    });
    $(".grid-small-large").each(function(){
        $(this).flexigrid({
            height:'350',
            width:'auto',
            striped:true,
            singleSelect: false,
            resizable: false,
            onSuccess: function() {
                addGrid($(".grid-small-large"), this);
            }
        });
    });
    formatTipsy();
}
formatGrids();

function formatButtons() {
    $(".button").button();
    $(".button-cancel").button({
        icons: {
            primary: "ui-icon-cancel"
        }
    });
    $(".button-confirm").button({
        icons: {
            secondary: "ui-icon-plus"
        }
    });
    $(".button-search").button({
        icons: {
            primary: "ui-icon-search"
        }
    });
    formatTipsy();
}
formatButtons();

function formatDecimalNumber(s) {

    // 50,00
    // 1.543,40 = 1543.00

    o = s.replace(".", "");
    o = o.replace(",",".");

    return o;
}

function formatValor(form, field) {

    $(form).find(field).blur(function(){
        formatValor(form, field);
    });

    $(form).find(field).parseNumber({
        format:"#,##0.00",
        locale:"br"
    });
    $(form).find(field).formatNumber({
        format:"#,##0.00",
        locale:"br"
    });
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function loadSearchProperties(objForm, ac) {

    $("span[class*='btnAcoesExi']").hide();
    $("span[class*='btnAcoesOcu']").show();

    var objDisplay = '';
    $(".btnToggleDataSearch").unbind('click').on('click', function(){

        objClick    = $(this);
        idClick     = objClick.parent().parent().parent().parent().parent().attr('id');

        objVal = $(this).attr("alt");

        sDiv = "div#opts-" + objVal;

        objBase = $('#' + idClick).find(sDiv);

        objDisplay = objBase.css("display");

        if (objDisplay === "block") {

            objBase.hide();
            objClick.find("span[class*='btnAcoesExi']").hide();
            objClick.find("span[class*='btnAcoesOcu']").show();
            objDisplay = 'none';

        } else {

            objClick.find("span[class*='btnAcoesOcu']").hide();
            objClick.find("span[class*='btnAcoesExi']").show();

            //objClick.find('.btnAcoesOcultarTitulo').hide();
            //objClick.find('.btnAcoesExibirTitulo').show();

            objBase.show();
            objDisplay = 'block';

        }

    });
    //objForm.find(".btnToggleDataSearch").trigger("click");

    $(".show-fields-all").toggle(function(){

        $("div[id^='opts-'").show();

        $('.btnAcoesExibirTitulo').show();
        $('.btnAcoesOcultarTitulo').hide();

        $(this).text("Ocultar");

    }, function(){

        $("div[id^='opts-'").hide();

        $('.btnAcoesOcultarTitulo').show();
        $('.btnAcoesExibirTitulo').hide();

        $(this).text("Exibir");

    });

    if (ac == undefined) {
        $(".btnToggleDataSearch").trigger("click");
    }

}
