
/*
 * Biblioteca para utilização da ferramenta de navegação denominada Navigator.
 *
 * HelpMe!
 */

function _sa_helpme_new(objectWindow, b) {
    
    $.get(_sa_baseUrl() + "opts/help/new", function(callback){
        
        $(objectWindow).html(callback);

        objForm = $(objectWindow).find("form");
        codApp = $("#sa-appname").val();
        if (codApp != "") {
            objForm.find("select[name='aplicativo']").find("option[value='" + codApp + "']").attr({"selected": "selected"});
        }
        objForm.find("textarea").focus();
        $(".button").button().css({"font-weight":"normal"});
        
        objForm.submit(function(){
            
            objForm.find("input[type='submit']").attr({"disabled":"disabled"}).val("Aguarde ...");
            
            serialize = objForm.serialize();
            serialize = _sa_helpme_serialize_report(serialize);

            $.post(_sa_baseUrl() + "opts/help/new", serialize, function(callback){
                
                if (callback.type == "sucess") {
                    b.hide();
                    $.jGrowl(callback.msg,{
                        header:"Atenção"
                    });
                } else if (callback.type == "error") {
                    $.jGrowl(callback.msg,{
                        header:"Atenção"
                    });
                    objForm.find("input[type='submit']").removeAttr("disabled").val("Abrir chamado");
                }

                return false;

            }, "json");

            return false;
            
        });
    });
}

function _sa_helpme_browser_name() {
    b = "";
    if (jQuery.browser.chrome) { b = "Google Chrome"; }
    if (jQuery.browser.safari) { b = "Safari"; }
    if (jQuery.browser.opera) { b = "Opera"; }
    if (jQuery.browser.msie) { b = "Internet Explorer"; }
    if (jQuery.browser.mozilla) { b = "Mozilla Firefox"; }
    return b;
}

function _sa_helpme_serialize_report(s) {

    var userAgent = navigator.userAgent.toLowerCase();

    // Figure out what browser is being used
    jQuery.browser = {
            version: (userAgent.match( /.+(?:rv|it|ra|ie|me)[\/: ]([\d.]+)/ ) || [])[1],
            chrome: /chrome/.test( userAgent ),
            safari: /webkit/.test( userAgent ) && !/chrome/.test( userAgent ),
            opera: /opera/.test( userAgent ),
            msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),
            mozilla: /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent )
    };
    
    browserVersion = jQuery.browser.version;
    browserName = _sa_helpme_browser_name();

    d = new Date();
    dateFormated = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

    s += "&r_bname=" + browserName;
    s += "&r_bversion=" + browserVersion;
    s += "&r_os=" + navigator.platform;
    
    s += "&r_appname=" + $("#sa-appname").val();
    s += "&r_appparams=" + $("#sa-appparams").val();

    if ($("#sa-appschema").val() != undefined) {
        s += "&r_appschema=" + $("#sa-appschema").val();
    }

    s += "&r_baseurl=" + $("#sa-baseurl").val();
    s += "&r_date_computer=" + dateFormated;

    return s;
    
}

function _sa_helpme_list(objectContent, b, status) {
    $.get(_sa_baseUrl() + "opts/help/list/" + status, function(callback){
        $(objectContent).html(callback);
        $(".grid-helpme-list").flexigrid({height:"345"});
        
        objectContent.find("table td").click(function(){
            idChamado = $(this).parent().attr("id");
            
            $.post(_sa_baseUrl() + "opts/help/chamados/details", {chamado: idChamado}, function(callback){

                b2 = new Boxy(callback,{
                    unloadOnHide:true, title: "Detalhes do chamado"
                });
                
                b2Content = b2.getContent();
                b2Content.find(".cancelar-chamado").click(function(){
                    objAlt = $(this).attr("alt");
                    wUrlCancelarChamado = _sa_baseUrl() + "opts/help/chamados/cancelar";
                    Boxy.confirm("Tem certeza que deseja cancelar o chamado ?", function(){
                        $.post(wUrlCancelarChamado, {chamado: objAlt}, function(callback){
                            if (callback.type == "sucess") {
                                $.jGrowl(callback.msg,{
                                    header:"Atenção"
                                });
                                b2.hide();
                                $(".helpme-left").find("a[rel='list-abertos']").trigger("click");
                            } else {
                                $.jGrowl(callback.msg,{
                                    header:"Atenção"
                                });
                            }
                            
                        }, "json");
                    }, {title: "Atenção"});
                });

                _sa_helpme_comentarios(idChamado, b2, b);

            });
        });

    });
}

function _sa_helpme_comentarios(chamado, objWindow, objBaseWindow) {
    
    $.post(_sa_baseUrl() + "opts/help/chamados/comentarios", {chamado: chamado}, function(callback){

        objWind = objWindow.getContent();
        $(objWind).find(".helpme-details-comentarios").html(callback);

        $(objWind).find(".reabrir-chamado").click(function(){

            id_chamado = $(this).attr("alt");
            
            Boxy.confirm("Tem certeza que deseja reabrir o chamado?", function() {
                
                $.post(_sa_baseUrl() + "opts/help/chamados/reabrir", {chamado: id_chamado}, function(callback){

                    if (callback.type == "sucess") {

                        $.jGrowl(callback.msg,{
                            header:"Atenção"
                        });

                        $(objWind).find("form").find("textarea").val("");

                        objWindow.hide();
                        //objBaseWindow.hide();

                        //$("#opt-helpme").trigger("click");
                        
                        $(".helpme-left").find("a[rel='list-abertos']").trigger("click");

                    } else {
                        $.jGrowl(callback.msg,{
                            header:"Atenção"
                        });
                    }

                }, "json");
            }, {title: 'Atenção'});
          
        });

        $(objWind).find("form").unbind("submit");
        
        $(objWind).find("form").submit(function(){
            
            objSerialize = $(this).serialize();
            
            $(objWind).find("form").find("input[type='submit']").attr({"disabled":"disabled"}).val("Aguarde ...");

            $.post(_sa_baseUrl() + "opts/help/chamados/comentarios/add", objSerialize, function(callback){
                
                if (callback.type == "sucess") {

                    $.jGrowl(callback.msg,{
                        header:"Atenção"
                    });
                    
                    _sa_helpme_comentarios(chamado, objWindow);

                    $(objWind).find("form").find("textarea").val("");
                    $(objWind).find("form").find("input[type='submit']").removeAttr("disabled").val("Enviar comentário");

                } else {
                    $.jGrowl(callback.msg,{
                        header:"Atenção"
                    });
                    $(objWind).find("form").find("input[type='submit']").removeAttr("disabled").val("Enviar comentário");
                }

            }, "json");
        });
    });

}