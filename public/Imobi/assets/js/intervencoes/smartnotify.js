(function($){
	
    function executeInterval(){
		
        $.post(optSmartNotify.pathSync,{},function(data){
			
            oSNthis = optSmartNotify.objThis;
			
            var dadosLi = new Array();
            var qntdDados = oSNthis.length;
			
            for(i=0; i < qntdDados; i++) {
                vOdata = jQuery(oSNthis[i]).attr("rel");
                dadosLi[vOdata] = oSNthis[i];
            }
            jQuery("div[id^='tip-notify-']").hide();
			
            jQuery.each(data,function(index,element){
				
                if (element.objArea in dadosLi) {
                    $i=0;
                    divLayerObjectId = jQuery(dadosLi[element.objArea]).find("div[id='tip-notify-" + element.objArea + "']").attr("id");
                    divLayer = jQuery(dadosLi[element.objArea]).find("div[id='tip-notify-" + element.objArea + "']");
                    if (divLayerObjectId != undefined) {
                        divLayer.text(element.qntd).show();
                    } else {
                        createLayer(optSmartNotify,element,jQuery(dadosLi[element.objArea]))
                    }
					
                }
            });
			
        },"json");
		
    }
	
    function createLayer(optSmartNotify,element,element2) {

        layer = document.createElement("div");
        jQuery(layer).attr({
            "id":"tip-notify-" + element.objArea
            });
        offSet = jQuery(element2).position();
        osn = optSmartNotify;
        $(layer).css({
            "background-color":osn.bgColor,
            "color":osn.textColor,
            "padding":"2px",
            "position":"absolute",
            "font-size": osn.textSize,
            "margin-left":parseFloat(offSet.left + 17),
            "margin-top":"8px",
            "-moz-border-radius":"3px",
            "-webkit-border-radius":"3px",
            "border-right": "1px solid " + osn.borderColor,
            "border-bottom": "1px solid " + osn.borderColor,
        }).text(element.qntd).hover(function(){
            jQuery(this).css({
                "background-color":osn.hoverColor
                });
        },function(){
            jQuery(this).css({
                "background-color":osn.bgColor
                });
        });
		
        jQuery(element2).prepend(layer);
        
    }
	
    $.fn.smartnotify = function(customOptionSmartNotify) {
		
        objHel = jQuery(this).attr("rel");

        if (objHel != undefined) {
		
            url = location.href;
            baseURL = $("#sa-baseurl").val();
		
            optionSmartNotify = {
                bgColor : "#ff0000",
                hoverColor : "#ff9418",
                borderColor : "#840000",
                textColor : "#fff",
                textSize : "9px",
                value : 99,
                boxTitle : jQuery(this).find("a").attr("alt"),
                pathSync : baseURL + "preferencias/notify/verificar",
                objThis : this,
                objHel : jQuery(this).attr("rel")
            };
			
            optSmartNotify = $.extend(optionSmartNotify,customOptionSmartNotify);
            objThis = jQuery(this);
        
            $.post(optSmartNotify.pathSync,{},function(data){

                    $.each(data,function(index,element){

                            $.each(objThis,function(index2,element2){

                                    if (element.objArea == jQuery(element2).attr("rel")) {
                                            createLayer(optSmartNotify,element,element2);
                                    }

                            });

                    });

            },"json");
	
        }
		
    setInterval(executeInterval,180000);
		
    };
	
})(jQuery);