@extends('PMTS._layout.header_footer')
@section('title', 'Estados')

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('sucesso'))
        <div class="alert alert-success">
            Cadastrado com sucesso!
        </div>
    @endif

    <div class="container-fluid ">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="height: 50px;">
                        <h3 class="panel-title" style="margin-top: 6px;">Estados</h3>
                    </div>
                    <div class="panel-body">
                        <form action="{{ url('uf/store') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="pkey" id="pkey" value="" />
                            <div class="modal-body">
                                <div class="elementGroup">
                                    <div class="form-group row">
                                        <div class="col-md-4"><label style="float: right;">* UF</label></div>
                                        <div class="col-md-6">
                                            <input type="text" name="unid_fed" class="form-control" maxlength="2">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4"><label style="float: right;">* Estado</label></div>
                                        <div class="col-md-6">
                                            <input type="text" name="estado" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            <a class="btn btn-default" href="{{ URL::to('uf') }}">Voltar</a>
                            <button type="submit" class="btn btn-primary">Cadastrar</button>                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection