-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03-Set-2017 às 23:31
-- Versão do servidor: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imobilab`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `imoveis`
--

CREATE TABLE `imoveis` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `valor` bigint(20) NOT NULL,
  `texto` text,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `imoveis`
--

INSERT INTO `imoveis` (`id`, `titulo`, `valor`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'teste', 123, NULL, '2017-09-03 21:40:50', '2017-09-03 21:40:50'),
(2, 'teste 2', 1234, NULL, '2017-09-03 21:41:01', '2017-09-03 21:41:01'),
(6, 'teste 123', 456, NULL, '2017-09-03 21:46:46', '2017-09-03 21:46:46'),
(7, 'xp', 50, NULL, '2017-09-03 21:47:29', '2017-09-03 22:02:49'),
(8, 'teste 4588', 456, NULL, '2017-09-03 21:56:01', '2017-09-03 22:00:46'),
(9, '12', 12, NULL, '2017-09-03 22:07:24', '2017-09-03 22:07:24'),
(12, 'Mais um númeroe', 333, NULL, '2017-09-03 22:39:40', '2017-09-03 22:39:40'),
(13, 'sssssss', 12, NULL, '2017-09-03 22:43:24', '2017-09-03 22:43:34'),
(14, 'testee 44', 4, NULL, '2017-09-03 23:10:51', '2017-09-03 23:10:51'),
(15, 'teste', 5, NULL, '2017-09-03 23:11:45', '2017-09-03 23:11:45'),
(16, 'teste', 6, NULL, '2017-09-03 23:11:53', '2017-09-03 23:11:53'),
(17, 'tste', 5, NULL, '2017-09-03 23:12:01', '2017-09-03 23:12:01'),
(18, 'xbbb', 1, NULL, '2017-09-03 23:12:14', '2017-09-03 23:12:14'),
(19, 'xbbb', 1, NULL, '2017-09-03 23:12:15', '2017-09-03 23:12:15'),
(20, 'xbbbbbb', 2, NULL, '2017-09-03 23:15:17', '2017-09-03 23:15:17'),
(21, 'xbbbbbb', 2, NULL, '2017-09-03 23:22:52', '2017-09-03 23:22:52'),
(22, 'xbbbbbb', 2, NULL, '2017-09-03 23:23:36', '2017-09-03 23:23:36'),
(23, 'xbbbbbb', 2, NULL, '2017-09-03 23:26:46', '2017-09-03 23:26:46'),
(24, 'xbbbbbb', 2, NULL, '2017-09-03 23:29:15', '2017-09-03 23:29:15'),
(25, 'xbbbbbb', 2, NULL, '2017-09-03 23:35:03', '2017-09-03 23:35:03'),
(26, 'teste', 5, NULL, '2017-09-03 23:47:14', '2017-09-03 23:47:14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Paulo', 'paulo.ferreira@trustit.com.br', '$2y$10$rCAGnW9E5G3DPZeT4hewL.QLEU7nzDLHz7OwVPCcZWmUCQnRhQO.m', 'wjIupzLdgXQNQvOsA1oOBtAmYQkyKXGXiGQxJNEv6ET8Zmi7gPmTd6rrPTCi', '2017-06-12 14:59:46', '2017-08-22 15:51:13'),
(6, 'Teste1', 'teste1@teste.com.br', '$2y$10$pAbWWIzcIANe6IYg0m960.94.1vJntS0SLjuzge/WVquI4AmXkYV.', '6WsWtsDJaZaS9xIh1jI4ywRTvy36CYhP76ilVjUmpYjdLgGvnOGyAPY5IH0d', '2017-06-12 16:11:03', '2017-06-12 16:11:03'),
(7, 'Teste2', 'Teste2@teste.com.br', '$2y$10$scX4.yBWnRZ7uIH9sXdxTuPr.snEnWl.1DnLRo8eVXgeWkg1o15gC', NULL, '2017-06-12 16:12:15', '2017-06-12 16:12:15'),
(8, 'teste3', 'teste3@teste.com.br', '$2y$10$ZfXB7lgFpjhxon4enF/uJuK/g.iGQcT34BnM28SlQKd2lgm9W4y5q', NULL, '2017-06-12 16:14:37', '2017-06-12 16:14:37'),


--
-- Indexes for dumped tables
--

--
-- Indexes for table `imoveis`
--
ALTER TABLE `imoveis`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `imoveis`
--
ALTER TABLE `imoveis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
