<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
       	Eloquent::unguard();

       	$this->call(UsersTableSeeder::class);

        /*
        DB::table('users')->delete();
    	User::create(array(
        	'name'     => 'Paulo Henrique',
        	'username' => 'paulohenry@hotmail.com',
       	 	'email'    => 'paulohenry@hotmail.com',
        	'password' => Hash::make('paulohenry@hotmail.com'),
    	)); */
    }
}
