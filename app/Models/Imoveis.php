<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Imoveis extends Model
{
    protected $table = 'imoveis';
	protected $primaryKey = 'id';
	protected $fillable = ['titulo','texto','valor'];
    
}
