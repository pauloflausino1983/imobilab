<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    protected $table = 'users';
	protected $primaryKey = 'id';
	protected $fillable = ['id','name','email','password','remember_token','created_at','updated_at','senha_criado_usuario'];
    
}
