<?php

namespace App\Http\Controllers\Imobi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Imoveis;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $imoveis = Imoveis::all();        
        $usuario_logado = Auth::user();
		
        return view('Imobi.home.home')->with(['usuario' => $usuario_logado, 'imoveis'=>$imoveis]);
		
		//return view('Imobi.home.home')->with(['usuario' => $usuario_logado]);
    }

}
